// pages/home/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    posts:0,
    nickName:'',
    avatarUrl:'',
    briefly:'',
    background:'',
    topheight:0,
    openid:wx.getStorageSync('openid'),
    settinghide:true,
    userinfo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({topheight:wx.getSystemInfoSync().statusBarHeight + 46})
    this.setData({
      openid:options.id
    })
    wx.cloud.callFunction({
      name:'getuserinfo',
      data:{id:this.data.openid}
    })
    .then(res=>{
      console.log(res.result)
        this.setData({
        posts:res.result.posts.list.length == 0 ? 0 : res.result.posts.list[0].posts,
        userinfo:res.result.data[0],
        nickName:res.result.data[0].nickName,
        avatarUrl:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/avatar/'+res.result.data[0]._openid+'_avatar.png',
        background:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/background/'+res.result.data[0]._openid+'_bg.png'
      })
    })
  },
  opensetting()
  {
    wx.navigateBack({
      delta: 0,
      success: (res) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})