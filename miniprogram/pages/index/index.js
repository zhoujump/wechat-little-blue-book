// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchhide:false,
    scrolltop:0,
    load:false,
    settinghide:true,
    topheight:0,
    page:1,
    post:[]
  },
  scroll(e)
  {
    if(e.detail.scrollTop >= 56)
    {
      this.setData({searchhide:true})
    }
    else
    {
      this.setData({searchhide:false})
    }
  },
  openpost(e)
  {
    wx.navigateTo({
      url: '/pages/post/index?id=' + e.currentTarget.dataset.id
    })
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    this.getposts()
  },
  top()
  {
    this.setData({
      page:1,
      post:[{},{},{},{},{},{},{},{}]
    })
    this.getposts()
  },
  getposts()
  {
    if(this.data.load == true)
      return
    this.setData({load:true})
    wx.cloud.callFunction({
      name:'getpost',
      data:{
        page:this.data.page,
        }
    })
    .then(res =>{
      console.log(res.result)
      if(res.result.data.length == 0)
      {

      }
      else
      {
        if(this.data.page == 1)
        {
          this.setData({
            post:[...res.result.data],
            page:this.data.page + 1
          })
        }
        else
        {
          this.setData({
            post:[...this.data.post,...res.result.data],
            page:this.data.page + 1
          })

        }
       
      }
      this.setData({load:false})
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  bottom() {
    console.log('bottom')
    this.getposts()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
      return {
        title: '小蓝书',
        imageUrl: '/images/share.png' // 图片 URL
      }
    
  }
})