// pages/write/index.js
let id = ''
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodhide:true,
    openid:'',
    op:0.6,
    goods:[],
    progressnum:0,
    progress:0,
    progressall:0,
    settinghide:true,
    toptitle:'未命名草稿',
    picStr:'',
    title:'',
    post:[
    ],
    topheight:0
  },
  addgoods(e)
  {
    console.log(e.currentTarget.dataset.index)
    this.setData({
      post:[...this.data.post,{type:'goods',src:e.currentTarget.dataset.index}]
    })
    this.mask()
  },
  titleInput()
  {
    if(this.data.title == '')
      this.setData({
        toptitle: '未命名草稿',
      })
    else
    this.setData({
      toptitle: '草稿：' + this.data.title,
    })
  },

  chosePic(){
    this.setData({
      op:0,
      settinghide:false,
    })
    wx.chooseMedia({
      mediaType:['image'],
      sourceType:['album','camera'],
      count:1,
      camera:'back',
      success:result =>
      {
        console.log(result.tempFiles[0].tempFilePath)
        this.setData({
          picStr:result.tempFiles[0].tempFilePath
        })
        this.setData({
          op:0.6,
          settinghide:true,
        })
      },
      fail:result =>
      {
        this.setData({
          op:0.6,
          settinghide:true,
        })
      }
    })
  },

  addimage()
  {
    this.setData({
      op:0,
      settinghide:false,
    })
    wx.chooseMedia({
      mediaType:['image'],
      sourceType:['album','camera'],
      count:1,
      camera:'back',
      success:result =>
      {
        console.log(result.tempFiles[0].tempFilePath)
        this.setData({
          post:[...this.data.post,{type:'image',src:result.tempFiles[0].tempFilePath}]
        })
        this.setData({
          op:0.6,
          settinghide:true,
        })
      },
      fail:result =>
      {
        this.setData({
          op:0.6,
          settinghide:true,
        })
      }
    })
  },

  addtext()
  {
    this.setData({
      post:[...this.data.post,{type:'text',src:''}]
    })
  },
  showgoods()
  {
    this.setData({goodhide:false})
  },
  mask()
  {
    this.setData({
      goodhide:true,
      settinghide:true
    })
  },
  del(e)
  {
    console.log(e.target.dataset.index)
    this.data.post.splice(e.target.dataset.index,1)
    this.setData({
      post:this.data.post
    })
  },

  input(e)
  {
    this.data.post[e.target.dataset.index].src = e.detail.value
    this.setData({
      post:this.data.post
    })
  },

  up(e)
  {
    [this.data.post[e.target.dataset.index],this.data.post[e.target.dataset.index - 1]]=[this.data.post[e.target.dataset.index - 1],this.data.post[e.target.dataset.index]]
    this.setData({
      post:this.data.post
    })
  },

  down(e)
  {
    [this.data.post[e.target.dataset.index],this.data.post[e.target.dataset.index + 1]]=[this.data.post[e.target.dataset.index + 1],this.data.post[e.target.dataset.index]]
    this.setData({
      post:this.data.post
    })
  },

  isok()
  {
    if(this.data.progress < this.data.progressall)
      {
        this.setData({
          progressnum:parseInt((this.data.progress / this.data.progressall) * 100),
          progress:this.data.progress + 1
        })
         console.log(this.data.progress,this.data.progressnum)
    }
    else
    {
      this.setData({
        progressnum:parseInt((this.data.progress / this.data.progressall) * 100),
        progress:this.data.progress + 1
      })
      console.log(this.data.progress,this.data.progressnum)
      this.setData({
          progressnum:0,
          progress:0,
          progressall:0,
          settinghide:true,
          toptitle:'未命名草稿',
          picStr:'',
          title:'',
          post:[
          ],
      })
      wx.navigateTo({
        url: '/pages/post/index?id=' + id
      })
      }
  },

  sent()
  {
    if(this.data.picStr == '')
    {
      wx.showToast({
        title: '请上传一个封面',
        icon: 'none',
        duration: 2000//持续的时间
      })
      return 0
    }
    if(this.data.title == '')
    {
      wx.showToast({
        title: '请输入文章标题',
        icon: 'none',
        duration: 2000//持续的时间
      })
      return 0
    }
    if(this.data.post.length == 0)
    {
      wx.showToast({
        title: '请输入正文',
        icon: 'none',
        duration: 2000//持续的时间
      })
      return 0
    }

    this.setData({settinghide:false})
    this.setData({
      progressall:this.data.post.length + 2,
      progress:0
    })

      wx.cloud.callFunction({
      name:'uploadpost',
      data:{
        type:'creat',
        title:this.data.title
      }
     })
     .then(res =>{
       id = res.result
        this.isok()
        this.data.post.forEach((element,index) => {
          if(element.type == 'image')
            {
                wx.cloud.uploadFile({
                filePath: element.src,
                cloudPath: 'postimage/' + res.result + '_' + index +'_post.png'
              })
              .then(res=>
                {
                  this.isok()
                })
              .catch(res =>{
                  this.isok()
              })
            }
          else if(element.type == 'goods')
          {
            this.data.post[index].src = this.data.goods[this.data.post[index].src]._id
            this.data.post[index].type = 'goods'
            this.isok()
          }
          else
          {
            this.isok()
          }
        })
    
        wx.cloud.uploadFile({
          filePath: this.data.picStr,
          cloudPath: 'postimage/' + res.result +'_cover.png'
        })
        .then(res =>
          {
            this.isok()
          })
    
          wx.cloud.callFunction({
          name:'uploadpost',
          data:{
            type:'post',
            post:this.data.post,
            id:res.result
          }
         }) 
         .then(res =>{
           console.log(res.result)
           this.isok()
         })

     })

  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    wx.cloud.callFunction({name:'getopenid'})
    .then(res =>{
      this.setData({
        openid:res.result.openid
      })

      wx.cloud.callFunction({
        name:'changegoods',
        data:{
          type:'get',
          page:1,
          openid:this.data.openid
        }
      })
      .then(res => {
        console.log(res)
        res.result.data.forEach((element,index) => {
          res.result.data[index].off = parseInt(((element.price - element.newprice) / element.price) * 100)
        });
        this.setData({
          goods:res.result.data
        })
      })

    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})