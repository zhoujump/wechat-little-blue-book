// pages/shop/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgsrc:'',
    off:0,
    shoptitle:'',
    shoptext:'',
    price:'',
    newprice:'',
    op:0,
    settinghide:true,
    changehide:true,
    topheight:0,
    openid:wx.getStorageSync('openid'),
    goods:[]
  },
  delgoods(e)
  {
    console.log(e.currentTarget.dataset.index)
    let index = e.currentTarget.dataset.index
    wx.showModal({
      title: '确定要删除这个商品吗',
      content: '“' + this.data.goods[index].shoptitle + '”将会被删除',
      complete: (res) => {
        if (res.cancel) {
          
        }
        if (res.confirm) {
          wx.cloud.callFunction({
            name:'changegoods',
            data:{
              type:'del',
              id:this.data.goods[index]._id
            }})
            .then(res => {
              wx.showToast({
                title: '删掉了',
              })
              this.onLoad()})}}})},
  chooseimg()
  {
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album', 'camera'],
      success:res => {
        console.log(res)
        this.setData({imgsrc:res.tempFiles[0].tempFilePath})
      }
    })
  },
  sent()
  {
    wx.cloud.callFunction({
      name:'changegoods',
      data:{
        type:'creat',
        price:this.data.price,
        newprice:this.data.newprice,
        shoptext:this.data.shoptext,
        shoptitle:this.data.shoptitle
      }
    })
    .then(res =>{
      wx.cloud.uploadFile({
        filePath: this.data.imgsrc,
        cloudPath: 'goodsimage/' + res.result._id + '_goods.png'
      })
      .then(res => {
        this.setData({
          imgsrc:'',
          off:0,
          shoptitle:'',
          shoptext:'',
          price:'',
          newprice:'',
          op:0.2,
          settinghide:true,
          topheight:0 
        })
        this.onLoad()
      })
    })
    
  },
  changegoods(e)
  {
    this.setData({
      changehide:false,
      shoptext:this.data.goods[e.currentTarget.dataset.index].shoptext,
      shoptitle:this.data.goods[e.currentTarget.dataset.index].shoptitle,
      price:this.data.goods[e.currentTarget.dataset.index].price,
      newprice:this.data.goods[e.currentTarget.dataset.index].newprice,
      id:this.data.goods[e.currentTarget.dataset.index]._id,
      imgsrc:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/goodsimage/' + this.data.goods[e.currentTarget.dataset.index]._id + '_goods.png'
    })
  },
  change()
  {
    wx.cloud.callFunction({
      name:'changegoods',
      data:{
        id:this.data.id,
        type:'update',
        price:this.data.price,
        newprice:this.data.newprice,
        shoptext:this.data.shoptext,
        shoptitle:this.data.shoptitle
      }
    })
    .then(res =>{
      if(this.data.imgsrc != 'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/goodsimage/' + this.data.id + '_goods.png')
      {
        wx.cloud.uploadFile({
          filePath: this.data.imgsrc,
          cloudPath: 'goodsimage/' + res.result._id + '_goods.png'
        })
      }
        this.setData({
          imgsrc:'',
          off:0,
          shoptitle:'',
          shoptext:'',
          price:'',
          newprice:'',
          changehide:true,
          settinghide:true,
        })
        this.onLoad()
    })
  },
  moregoods(e)
  {
    wx.showModal({
      title: '商品补货',
      placeholderText: '输入补货数量,下架填负数',
      editable:true,
      complete: (res) => {
        if (res.confirm) {
          wx.cloud.callFunction({
            name:'changegoods',
            data:{
              id:this.data.goods[e.currentTarget.dataset.index]._id,
              type:'add',
              num:res.content
            }
          })
          .then(res => {
            this.onLoad()
          })
        }
      }
    })
  },
  addgoods()
  {
    this.setData({
      settinghide:false,
    })
  },
  hidebox()
  {
      this.setData({
      changehide:true,
      settinghide:true,
      shoptext:'',
      shoptitle:'',
      price:'',
      newprice:'',
      imgsrc:''
      })
  },
  pricechange()
  {
    this.setData({off:parseInt(((this.data.price - this.data.newprice) / this.data.price) * 100)})
  },
  back()
  {
    wx.navigateBack({
      delta: 0,
      success: (res) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
   
    wx.cloud.callFunction({
      name:'changegoods',
      data:{
        type:'get',
        page:1,
        openid:this.data.openid
      }
    })
    .then(res => {
      console.log(res)
      res.result.data.forEach((element,index) => {
        res.result.data[index].off = parseInt(((element.price - element.newprice) / element.price) * 100)
      });
      this.setData({
        goods:res.result.data
      })
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})