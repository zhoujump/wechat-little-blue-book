// pages/land/index.js
const defaultAvatarUrl = '/images/logo.png'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    type:'',
    id:'',
    topheight:0,
    state:'login',
    avatarUrl: defaultAvatarUrl,
    nickName:'',
    openid:''
  },
  onChooseAvatar(e) {
    this.setData({
      avatarUrl:e.detail.avatarUrl
    })
  },
  formSubmit(e){
     wx.cloud.uploadFile({
       filePath: this.data.avatarUrl,
       cloudPath: 'avatar/' + this.data.openid+'_avatar.png'
     })
     .then(res =>{
      wx.cloud.callFunction({
        name:'signup',
        data:{
         nickName:this.data.nickName
        }
       })
       wx.switchTab({
         url: '/pages/index/index',
       })
     })
       
        
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    wx.cloud.callFunction({name:'getopenid'})
    .then(res =>{
      this.setData({
        openid:res.result.openid
      })
    })
    .catch(res =>{
      wx.showToast({
        title: '失败',
        icon: 'error',
        duration: 2000//持续的时间
      })
      console.log(res)})
    
    wx.cloud.callFunction({name:'login'})
    .then(res =>{
      console.log(res)
      if(res.result.data.length != 0)
      {
        wx.setStorageSync('openid', res.result.data[0]._openid)
        console.log('ok')
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
      else
      {
          this.setData({
              state:'signup'
          })
      }
    })
    .catch(res =>{
      wx.showToast({
        title: '失败',
        icon: 'error',
        duration: 2000//持续的时间
      })
      console.log(res)
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})