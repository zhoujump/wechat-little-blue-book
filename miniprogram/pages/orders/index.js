// pages/orders/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    settinghide:true,
    choss:true,
    buy:[],
    sell:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    wx.cloud.callFunction({
      name:'order',
      data:{type:'get'}
    })
    .then(res =>
      {
        console.log(res)
        this.setData({
          buy:res.result.buy.data,
          sell:res.result.sell.data,
        })
      })
  },
  comf(e)
  {
    let index = e.currentTarget.dataset.index
    wx.showModal({
      title: '确认收货',
      content: '确认'+this.data.buy[index].shoptitle+'已经收货',
      success: res =>{
        wx.cloud.callFunction({
          name:'order',
          data:{
            type:'comf',
            id:this.data.buy[index]._id
          }
        })
        .then(res => {
          this.onLoad()
        })
      }
    })
  },
  sent(e)
  {
    let index = e.currentTarget.dataset.index

    wx.showModal({
      title: '确认发货',
      content: '确认'+this.data.sell[index].shoptitle+'已经发货',
      success: res =>{
        wx.cloud.callFunction({
          name:'order',
          data:{
            type:'sent',
            id:this.data.sell[index]._id
          }
        })
        .then(res => {
          this.onLoad()
        })
      }
    })
  },
  choss1()
  {
    this.setData({choss:true})
  },
  choss2()
  {
    this.setData({choss:false})
  },
  back()
  {
    wx.navigateBack({
      delta: 0,
      success: (res) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})