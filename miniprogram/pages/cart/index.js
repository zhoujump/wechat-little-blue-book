// pages/cart/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading:false,
    pross:0,
    totalprice:0,
    totalnewprice:0,
    settinghide:true,
    topheight:0,
    cartlist:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
    this.setData({
      totalprice:0,
    totalnewprice:0,
    })
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    wx.cloud.callFunction({
      name:'cart',
      data:{
        type:'get'
      }
    })
    .then(res =>{
      this.setData({cartlist:res.result.data})
      console.log(res.result.data)
    })
  },
  count()
  {
    if(this.data.pross == 1)
    {
      this.setData({
        loading:false,
        pross:0,
        totalprice:0,
        totalnewprice:0})
      this.onLoad()
    }
    this.setData({pross:this.data.pross - 1})
  },
  pay()
  {
    
    wx.cloud.callFunction({
      name:'order',
      data:{
        type:'pay',
        price:this.data.totalnewprice
      }
    })
    .then(res => {
      console.log(res)
    })



  },
  checkout()
  {
    this.setData({
      loading:true,
      pross:this.data.cartlist.length,
    })
    this.data.cartlist.forEach(element => {
      if(element.check == true)
      {
        wx.cloud.callFunction({
          name:'order',
          data:{
            type:'add',
            id:element._id
          }
        })
        .then(res =>{
          if(res.result == '余额不足')
          {
            wx.showToast({
              title: '余额不足，'+element.shoptitle+'购买失败',
              icon: 'none',
              duration: 2000,
            })
          }
          this.count()
        })
        
      }
      else
      {
        this.count()
      }
    });
  },
  check(e)
  {
    let index = e.currentTarget.dataset.index
    if(this.data.cartlist[index].check == true)
      {
        this.data.cartlist[index].check = false
        this.setData({
          totalprice:this.data.totalprice - (this.data.cartlist[index].price - 0),
          totalnewprice:this.data.totalnewprice - (this.data.cartlist[index].newprice - 0),
        })
      }
    else
      {
        this.data.cartlist[index].check = true
        this.setData({
          totalprice:this.data.totalprice + (this.data.cartlist[index].price - 0),
          totalnewprice:this.data.totalnewprice + (this.data.cartlist[index].newprice - 0),
        })
      }

    this.setData({cartlist:this.data.cartlist})
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})