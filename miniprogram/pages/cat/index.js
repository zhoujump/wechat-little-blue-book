// pages/cat/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    charge:false,
      scrollTop:0,
    chouka:false,
    update:false,
    autowork:false,
    wait:64,
    job:'home',
    state:'loading',
    openid:wx.getStorageSync('openid'),
    settinghide:true
  },
  zhuangbei1()
  {
    if(this.data.item[this.data.itemindex]._id == this.data.cat.caowei2)
    {
      wx.showToast({
        title: '已经在另一个槽位装备了',
        icon: 'none',
        duration: 2000,
      })
      return 0
    }
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'zhuangbei',
        id:this.data.item[this.data.itemindex]._id,
        caowei:'caowei1'
      }
    })
    .then(res =>{
      this.onLoad()
    })
  },
  zhuangbei2()
  {
    if(this.data.item[this.data.itemindex]._id == this.data.cat.caowei1)
    {
      wx.showToast({
        title: '已经在另一个槽位装备了',
        icon: 'none',
        duration: 2000,
      })
      return 0
    }
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'zhuangbei',
        id:this.data.item[this.data.itemindex]._id,
        caowei:'caowei2'
      }
    })
    .then(res =>{
      this.onLoad()
    })
  },
  charge(e)
  {
    console.log(e.currentTarget.dataset.price)

    wx.showModal({
      title: '充值',
      content:'充值'+e.currentTarget.dataset.price*100+'猫猫币',
      confirmText:'充值',
      complete: (res) => {
        if (res.cancel) {
          
        }
    
        if (res.confirm) {
          wx.cloud.callFunction({
            name:'order',
            data:{
              type:'kejin',
              price:e.currentTarget.dataset.price
            }
          })
          .then(res =>{
            if(res.result == '余额不足')
            {
              wx.showToast({
                title: '余额不足',
                icon: 'none',
                duration: 2000,
              })
            }
            else
            {
              this.setData({
                getitem:res.result,
                chouka:true
              })
              this.onLoad()
            }
          })
        }
      }
    })
  },
  kejin()
  {
    this.setData({charge:true})
  },
  closekejin()
  {
    this.setData({charge:false})
  },
  sellzhuangbei()
  {
    wx.cloud.callFunction({
        name:'cat',
        data:{
            type:'sell',
            id:this.data.item[this.data.itemindex]._id
        }
    })
    .then(res =>{
        console.log(res)
        this.setData({
            getitem:res.result,
            chouka:true
        })
        this.onLoad()
    })
  },
  useitem()
  {
      if(this.data.item[this.data.itemindex].id == 'fubenquan')
      {
          this.goadv()
          return 0
      }
    wx.cloud.callFunction({
        name:'cat',
        data:{
            type:'use',
            id:this.data.item[this.data.itemindex]._id
        }
    })
    .then(res =>{
        console.log(res)
        this.setData({
            getitem:res.result,
            chouka:true
        })
        this.onLoad()
    })
  },
  chositem(e)
  {
    this.setData({itemindex:e.currentTarget.dataset.index})
    console.log(e.currentTarget.dataset.index)
  },
  getitem()
  {
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'getitem'
      }
    })
    .then(res =>{
      this.setData({item:res.result.data})
    })
  },
  update()
  {
    switch (this.data.job) {
      case 'home':
        if(this.data.cat.lv * this.data.cat.lv <= this.data.cat.coin)
        {
          wx.cloud.callFunction({
            name:'cat',
            data:{
              type:'update',
              coin:-this.data.cat.lv * this.data.cat.lv,
              lv:1,
              fishlv:0,
              canlv:0,
              coinlv:0
            }
          })
          .then(res =>{
            this.onLoad()
          })
          this.closebut()
        }
        else
        {
          return 0
        }
        break;
        
        case 'fish':
        if(this.data.cat.fishlv * this.data.cat.fishlv <= this.data.cat.coin)
        {
          wx.cloud.callFunction({
            name:'cat',
            data:{
              type:'update',
              coin:-this.data.cat.fishlv * this.data.cat.fishlv,
              lv:0,
              fishlv:1,
              canlv:0,
              coinlv:0
            }
          })
          .then(res =>{
            this.onLoad()
          })
          this.closebut()
        }
        else
        {
          return 0
        }
        break;

        case 'cook':
        if(this.data.cat.canlv * this.data.cat.canlv <= this.data.cat.coin)
        {
          wx.cloud.callFunction({
            name:'cat',
            data:{
              type:'update',
              coin:-this.data.cat.canlv * this.data.cat.canlv,
              lv:0,
              fishlv:0,
              canlv:1,
              coinlv:0
            }
          })
          .then(res =>{
            this.onLoad()
          })
          this.closebut()
        }
        else
        {
          return 0
        }
        break;

        case 'shop':
        if(this.data.cat.coinlv * this.data.cat.coinlv <= this.data.cat.coin)
        {
          wx.cloud.callFunction({
            name:'cat',
            data:{
              type:'update',
              coin:-this.data.cat.coinlv * this.data.cat.coinlv,
              lv:0,
              fishlv:0,
              canlv:0,
              coinlv:1
            }
          })
          .then(res =>{
            this.onLoad()
          })
          this.closebut()
        }
        else
        {
          return 0
        }
        break;

      default:
        break;
    }
  },
  closechouka()
  {
    this.setData({chouka:false})
  },
  chouka()
  {
    if(this.data.cat.coin >= 1000 && this.data.item.length <= 99)
    {
      this.data.cat.coin = this.data.cat.coin - 1000
      this.setData({cat:this.data.cat})
      wx.cloud.callFunction({
        name:'cat',
        data:{
          type:'chouka'
        }
      })
      .then(res =>{
        this.setData({getitem:res.result,chouka:true})
        this.onLoad()
      })
    }
  },
  openupdate()
  {
    this.setData({update:true})
  },
  closebut()
  {
    this.setData({update:false})
  },
  goshop()
  {
    this.setData({job:'shop'})
  },
  gocook()
  {
    this.setData({job:'cook'})
  },
  goadv()
  {
    this.data.item.forEach(element => {
      if(element._id == this.data.cat.caowei1)
      {
        console.log(element)
        this.setData({zhuangbei1:element})
      }
      if(element._id == this.data.cat.caowei2)
      {
        this.setData({zhuangbei2:element})
      }
    });
    this.setData({job:'adv'})
  },
  gofish()
  {
    this.setData({job:'fish'})
  },
  gohome()
  {
    this.setData({job:'home'})
  },
  comf()
  {
    this.setData({state:'play'})
  },
  creat()
  {
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'creat'
      } 
    })
    .then(res =>{
      console.log(res.result)
      this.get()
    })
  },
  get()
  {
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'get',
       openid:this.data.openid
      }      
    })
    .then(res =>{
      this.setData({cat:res.result.data[0]})
    })
  },
  reget()
  {
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'reget'
      } 
    })
    .then(res =>{
      console.log(res.result)
      this.get()
    })
  },
  work()
  {
    if(this.data.wait == 0)
    {
      if(this.data.job == 'home')
      {
        return 0
      }
      if(this.data.job == 'fish')
      {
        this.setData({
        fish : this.data.cat.lv * this.data.cat.fishlv ,
        can : 0,
        coin : 0
        })
      }
      if(this.data.job == 'cook')
      {
        if(this.data.cat.fish < this.data.cat.lv * this.data.cat.canlv)
        {
          this.setData({autowork:false})
          return 0
        }
        this.setData({
        fish : -(this.data.cat.lv * this.data.cat.canlv),
        can : this.data.cat.lv * this.data.cat.canlv,
        coin : 0,
        })
      }
      if(this.data.job == 'shop')
      {
        if(this.data.cat.can < this.data.cat.lv * this.data.cat.coinlv)
        {
          this.setData({autowork:false})
          return 0
        }
        this.setData({
        fish : 0,
        can : -(this.data.cat.lv * this.data.cat.coinlv),
        coin : this.data.cat.lv * this.data.cat.coinlv
        })
      }
      this.setData({wait:64})
      wx.cloud.callFunction({
        name:'cat',
        data:{
          type:'work',
          fish:this.data.fish,
          can:this.data.can,
          coin:this.data.coin
        }
      })
      .then(res => {
        this.onLoad()
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setTabBarStyle({
      backgroundColor: '#deb887',
    })
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    this.getitem()
    wx.cloud.callFunction({
      name:'cat',
      data:{
        type:'get',
       openid:this.data.openid
      }      
    })
    .then(res =>{
      if(res.result.data.length == 0)
      {
        this.setData({state:'choose'})
        this.creat()
      }
      else
      {
        this.setData({state:'play'})

      }
      this.setData({cat:res.result.data[0]})
    })
  },
  autowork()
  {
    this.setData({autowork:!this.data.autowork})
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    setInterval(() => {
      if(this.data.wait > 0)
      {
        this.setData({wait:this.data.wait - 1})
      }
      else
      {
          if(this.data.job == 'home' || this.data.job == 'adv')
          {
            this.setData({autowork:false})
            return 0
          }
        if(this.data.job == 'cook' && this.data.autowork && this.data.cat.fish < this.data.cat.lv * (this.data.cat.lv - 1) + this.data.cat.canlv * this.data.cat.lv)
          {
            this.setData({autowork:false})
            return 0
          }
        if(this.data.autowork && this.data.cat.fish >= this.data.cat.lv * (this.data.cat.lv - 1) && this.data.job != 'home')
        {
          wx.cloud.callFunction({
            name:'cat',
            data:{
              type:'work',
              fish:- this.data.cat.lv * (this.data.cat.lv - 1),
              can:0,
              coin:0
            }
          })
          .then(res =>{
            this.onLoad()
          })
          this.work()
        }
        else
        {
          this.setData({autowork:false})
        }
      }
    }, 200);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    wx.setTabBarStyle({
      backgroundColor: '#ffffff',
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})