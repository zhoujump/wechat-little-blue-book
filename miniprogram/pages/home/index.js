// pages/home/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    posts:0,
    goods:0,

    nickName:'',
    avatarUrl:'',
    briefly:'',
    background:'',
    topheight:0,
    openid:wx.getStorageSync('openid'),
    settinghide:true,
    userinfo:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({topheight:wx.getSystemInfoSync().statusBarHeight + 46})
    wx.cloud.callFunction({name:'login'})
    .then(res =>{
    console.log(res)
    this.setData({
      userinfo:res.result.data[0],
      nickName:res.result.data[0].nickName,
      avatarUrl:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/avatar/'+res.result.data[0]._openid+'_avatar.png',
      background:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/background/'+res.result.data[0]._openid+'_bg.png'
    })
    wx.cloud.callFunction({
      name:'getuserinfo',
      data:{id:this.data.openid}
    })
    .then(res=>{
      console.log(res.result)
      this.setData({
        posts:res.result.posts.list[0].posts,
        goods:res.result.goods.list[0].goods
      })
    })
  })
  },
  tixian()
  {
    wx.showModal({
      title: '提现',
      placeholderText:'提现金额(元)',
      editable:true,
      confirmText:'提现',
      complete: res => {
        if (res.cancel) {
          
        }
    
        if (res.confirm) {
          if(res.content == '' || res.content - 0 < 0 || res.content - 0 > this.data.userinfo.money)
            {wx.showToast({
              title: '请检查输入金额',
              icon: 'none',
              duration: 2000,
            })
              return 0}
          console.log(res.content)
          wx.cloud.callFunction({
            name:'order',
            data:{
              type:'charge',
              price:- res.content - 0
            }
          })
          .then(res =>{
            this.onLoad()
          })
        }
      }
    })

  },
  chongzhi()
  {
    wx.showModal({
      title: '充值',
      placeholderText:'充值金额(元)',
      editable:true,
      confirmText:'充值',
      complete: (res) => {
        if (res.cancel) {
          
        }
    
        if (res.confirm) {
          if(res.content == '' || res.content - 0 < 0)
          {wx.showToast({
            title: '请检查输入金额',
            icon: 'none',
            duration: 2000,
          })
            return 0}
          console.log(res.content)
          wx.cloud.callFunction({
            name:'order',
            data:{
              type:'charge',
              price:res.content - 0
            }
          })
          .then(res =>{
            this.onLoad()
          })
        }
      }
    })
  },
  goshop()
  {
    wx.navigateTo({
      url: '/pages/shop/index',
      routeType: 'routeType',
      success: (result) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  goorder()
  {
    wx.navigateTo({
      url: '/pages/orders/index',
      routeType: 'routeType',
      success: (result) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  opensetting()
  {
    this.setData({settinghide:!this.data.settinghide})
  },
  setbackground()
  {
    wx.chooseMedia({
      mediaType:['image'],
      sourceType:['album','camera'],
      count:1,
      camera:'back',
      success:result =>
      {
        console.log(result.tempFiles[0].tempFilePath)
        this.setData({
          background:result.tempFiles[0].tempFilePath
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  onChooseAvatar(e) {
    this.setData({
      avatarUrl:e.detail.avatarUrl
    })
  },
  formSubmit(e){
    if(this.data.avatarUrl != 'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/avatar/'+this.data.openid+'_avatar.png')
    { wx.cloud.uploadFile({
       filePath: this.data.avatarUrl,
       cloudPath: 'avatar/' + this.data.openid+'_avatar.png'
     })}

     if(this.data.background != 'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/background/'+this.data.openid+'_bg.png')
     {wx.cloud.uploadFile({
      filePath: this.data.background,
      cloudPath: 'background/' + this.data.openid+'_bg.png'
    })}

    if(this.data.nickName == '')
      this.setData({nickName:this.data.userinfo.nickName})
    if(this.data.briefly == '')
      this.setData({briefly:this.data.userinfo.briefly})

    wx.cloud.callFunction({
      name:'changeinfo',
      data:{
      nickName:this.data.nickName,
      briefly:this.data.briefly
      }
    })
    this.opensetting()
    wx.cloud.callFunction({name:'login'})
    .then(res =>{
    console.log(res)
    this.setData({
      userinfo:res.result.data[0],
      nickName:this.data.userinfo.nickName,
      avatarUrl:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/avatar/'+this.data.openid+'_avatar.png',
      background:'cloud://study-4glnqttt963fc25b.7374-study-4glnqttt963fc25b-1322406209/background/'+this.data.openid+'_bg.png'
    })
    })

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})