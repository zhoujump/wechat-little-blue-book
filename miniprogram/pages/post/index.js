// pages/post/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loding:false,
    user:'',
    posts:0,
    post:[],
    settinghide:true,
    topheight:0,
    comm:'',
    commlist:[],
    commpage:1
  },
  addtocart(e)
  {
    let index = e.currentTarget.dataset.index
    wx.cloud.callFunction({
        name:'cart',
        data:{
            type:'add',
            id:this.data.post.post[index]._id
        }
    })
    .then(res =>{
        wx.showToast({
          title: '已添加购物车',
          duration: 2000,
          success: (res) => {},
          fail: (res) => {},
          complete: (res) => {},
        })
        this.data.post.post[index].cart = true
         this.setData({post:this.data.post})
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  sentcomm()
  {
      if(this.data.comm == '')
      {
        wx.showToast({
            title: '请输入评论',
            icon: 'none',
            duration: 2000//持续的时间
          })
          return 0
      }
    wx.cloud.callFunction({
        name:'sentcomm',
        data:{
            type:'sent',
            res:this.data.comm,
            postid:this.data.post._id,
          }
      })
      .then(res=>{
        this.setData({loding:true})
        wx.cloud.callFunction({
          name:'sentcomm',
          data:{
              type:'get',
              postid:this.data.post._id,
              page:1
            }
        })
        .then(res => {
          this.setData({loding:false})
          console.log(res)
          res.result.data.forEach((element,index) => {
            wx.cloud.callFunction({
              name:'getuserinfo',
              data:{
                id:element._openid,
                }
            })
            .then(result=>{
              console.log(result.result.data[0].nickName)
              res.result.data[index].name = result.result.data[0].nickName
              this.setData({
                commlist:this.data.commlist
              }) 
            })
            
          });
          this.setData({commlist:[...res.result.data]})
        })
      })
      wx.showToast({
        title: '评论成功',
        icon: 'none',
        duration: 2000//持续的时间
      })
      this.setData({comm:''})
  },
  onLoad(options) {
    this.setData({topheight:wx.getMenuButtonBoundingClientRect().top + 44})
    console.log(options)
    wx.cloud.callFunction({
      name:'getonepost',
      data:{
        id:options.id,
        }
    })
    .then(res =>{
      res.result.data.post.forEach((element,index) => {
        if(element.type == 'goods')
        {
          wx.cloud.callFunction({
            name:'changegoods',
            data:{
              id:element.src,
              }
          })
          .then(res =>{
            this.data.post.post[index] = res.result.data[0]
            this.data.post.post[index].type = 'goods'
            this.data.post.post[index].off = parseInt(((this.data.post.post[index].price - this.data.post.post[index].newprice) / this.data.post.post[index].price) * 100)
            this.setData({
              post:this.data.post
            })
          })
        }
      });
      this.setData({
        post:res.result.data
      })
      wx.cloud.callFunction({
        name:'getuserinfo',
        data:{
          id:res.result.data._openid,
          }
      })
      .then(res =>{
        this.setData({
          user:res.result.data[0],
          posts:res.result.posts.list.length == 0 ? 0 : res.result.posts.list[0].posts
        })
      })
      this.setData({loding:true})
      wx.cloud.callFunction({
        name:'sentcomm',
        data:{
            type:'get',
            postid:this.data.post._id,
            page:this.data.commpage
          }
      })
      .then(res => {
        this.setData({loding:false})
        if(res.result.data.length == 0)
        {
          return 0
        }
        else
        {
          res.result.data.forEach((element,index) => {
            wx.cloud.callFunction({
              name:'getuserinfo',
              data:{
                id:element._openid,
                }
            })
            .then(result=>{
              console.log(result.result.data[0].nickName)
              res.result.data[index].name = result.result.data[0].nickName
              this.setData({
                commlist:this.data.commlist
              }) 
            })
            
          });
          console.log(res)
           this.setData({
             commlist:[...this.data.commlist,...res.result.data],
             commpage:this.data.commpage + 1
            })
        }
        
      })
    })

    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },
  back()
  {
    wx.navigateBack({
      delta: 0,
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    this.setData({loding:true})
    console.log('la')
    wx.cloud.callFunction({
      name:'sentcomm',
      data:{
          type:'get',
          postid:this.data.post._id,
          page:this.data.commpage
        }
    })
    .then(res => {
      this.setData({loding:false})
      if(res.result.data.length == 0)
      {
        return 0
      }
      else
      {
        res.result.data.forEach((element,index) => {
          wx.cloud.callFunction({
            name:'getuserinfo',
            data:{
              id:element._openid,
              }
          })
          .then(result=>{
            console.log(result.result.data[0].nickName)
            res.result.data[index].name = result.result.data[0].nickName
            this.setData({
              commlist:this.data.commlist
            }) 
          })
          
        });
        console.log(res)
         this.setData({
           commlist:[...this.data.commlist,...res.result.data],
           commpage:this.data.commpage + 1
          })
      }
      
    })
  },
  viewuser(e)
  {
    wx.navigateTo({
      url: '/pages/user/index?id='+e.currentTarget.dataset.id,
      routeType: 'routeType',
      success: (result) => {},
      fail: (res) => {},
      complete: (res) => {},
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})