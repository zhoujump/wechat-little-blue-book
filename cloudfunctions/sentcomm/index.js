// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  if(event.type == 'sent')
  {
    await db.collection('comments').add({
        data: {
            res:event.res,
          postid:event.postid,
          _openid:openid
        }
      })
      return 'ok'
  }
  else
  {
    return db.collection('comments')
    .where({
        postid:event.postid
      })
    .skip((event.page - 1) * 10 + 1)
    .limit(10)
    .get()
  }

  
  
}