// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  if(event.openid != null)
  {
    return db.collection('posts')
    .where({_openid:event.openid})
    .orderBy('_id', 'asc')
    .skip((event.page - 1) * 10 + 1)
    .limit(10)
    .get()
  }
  else if(event.keyword != null)
  {

  }
  else
  {
    return db.collection('posts')
    .skip((event.page - 1) * 10 + 1)
    .orderBy('_id', 'asc')
    .limit(10)
    .get()
  }
}
