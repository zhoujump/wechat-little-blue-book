// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  let user = ''
  user = await db.collection('userinfo')
  .where({
    _openid:event.id
  })
  .get()
  
  user.goods = await db.collection('goods')
  .aggregate()
  .match({_openid:event.id})
  .count('goods')
  .end()

  user.posts = await db.collection('posts')
  .aggregate()
  .match({_openid:event.id})
  .count('posts')
  .end()

  return user
}
