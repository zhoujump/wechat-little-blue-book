// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  if(event.type == 'creat')
  {

    let cat = await db.collection('cat')
    .where({
        _openid:openid
      })
    .get()

    if(cat.data.length == 0)
    {
      await db.collection('cat')
      .add({
        data: {
          lv:1,
          fish:0,
          can:0,
          coin:0,
          fishlv:1,
          canlv:1,
          coinlv:1,
          pifucolor:Math.floor(Math.random() * 360),
          huawen:Math.floor(Math.random() * 3),
          huawencolor:Math.floor(Math.random() * 360),
          zhuangshi:Math.floor(Math.random() * 5),
          zhuangshicolor:Math.floor(Math.random() * 360),
          _openid:openid
        }
      })
      return 'ok'
    }
    else
    {
      return 'ishave'
    }
   
  }
  else if(event.type == 'get')
  {
    return db.collection('cat')
    .where({
        _openid:event.openid
      })
    .get()
  }
  else if(event.type == 'reget')
  {
    await db.collection('cat')
      .where({_openid:openid})
      .update({
        data: {
          pifucolor:Math.floor(Math.random() * 360),
          huawen:Math.floor(Math.random() * 3),
          huawencolor:Math.floor(Math.random() * 360),
          zhuangshi:Math.floor(Math.random() * 4),
          zhuangshicolor:Math.floor(Math.random() * 360),
        }
      })
    return 'ok'
  }
  else if(event.type == 'work')
  {
    await db.collection('cat')
      .where({_openid:openid})
      .update({
        data: {
          fish:db.command.inc(event.fish),
          can:db.command.inc(event.can),
          coin:db.command.inc(event.coin),
        }
      })
    return 'ok'

  }
  else if(event.type == 'update')
  {
    await db.collection('cat')
      .where({_openid:openid})
      .update({
        data: {
          coin:db.command.inc(event.coin),
          lv:db.command.inc(event.lv),
          fishlv:db.command.inc(event.fishlv),
          canlv:db.command.inc(event.canlv),
          coinlv:db.command.inc(event.coinlv),
        }
      })
    return 'ok'
  }
  else if(event.type == 'chouka')
  {

    await db.collection('cat')
      .where({_openid:openid})
      .update({
        data: {
          coin:db.command.inc(-1000),
        }
      })
    let ittype = ''
    let name = ''
    let id = ''
    let rad = Math.floor(Math.random() * 11)
    let atk = 0
    let def = 0
    let hp = 0
    let tatk = 0
    let tatkp = 0
    switch (rad) {
      case 0:
      case 1:
      case 2:
      case 3:
        ittype = 'item'
        name = '一箱小鱼干'
        id = 'fishbox'
        break;
      case 4:
      case 5:
      case 6:
        ittype = 'item'
        name = '一箱鱼罐头'
        id = 'canbox'
        break;
      case 7:
      case 8:
        ittype = 'item'
        name = '一箱金币'
        id = 'coinbox'
        break;
      case 9:
        ittype = 'item'
        name = '挑战券'
        id = 'fubenquan'
        break;
      default:
        ittype = 'zhuangbei'
        id = 'zhuangbei' + Math.floor(Math.random() * 6)
        switch (id) {
          case 'zhuangbei0':
            name = '猫猫配枪'
            break;
          case 'zhuangbei1':
            name = '燕尾猫猫服'
            break;
          case 'zhuangbei2':
            name = '斗志猫猫头巾'
            break;
          case 'zhuangbei3':
            name = '猫猫尾帽'
            break;
          case 'zhuangbei4':
            name = '猫猫墨镜'
            break;
          case 'zhuangbei5':
            name = '小鱼干'
            break;
          case 'zhuangbei6':
            name = '龙图面妆'
            break;
          default:
            break;
        }
        
        let lv =  Math.floor(Math.random() * 4) + 1
        for (lv; lv > 0; lv--) {
          let shuxing = Math.floor(Math.random() * 5)
          switch (shuxing) {
            case 0:
              atk = atk + Math.floor(Math.random() * 101)
              break;
            case 1:
              def = def + Math.floor(Math.random() * 101)
              break;
            case 2:
              hp = hp + Math.floor(Math.random() * 101)
              break;
            case 3:
              tatk = tatk + Math.floor(Math.random() * 101)
              break;
            case 4:
              tatkp = tatkp + Math.floor(Math.random() * 101)
              break;
            default:
              break;
          }
        }      
        break;
    }
    if(ittype == 'zhuangbei')
    {
      let color = Math.floor(Math.random() * 360)
      await db.collection('nftitem')
      .add({
        data: {
          type:ittype,
          name:name,
          id:id,
          color:color,
          atk:atk,
          def:def,
          hp:hp,
          tatk:tatk,
          tatkp:tatkp,
          _openid:openid
        }
      })
      return {
        type:ittype,
        name:name,
        id:id,
        color:color,
        atk:atk,
        def:def,
        hp:hp,
        tatk:tatk,
        tatkp:tatkp,
        _openid:openid
      }
    }
    else
    {
      await db.collection('nftitem')
      .add({
        data: {
          type:ittype,
          name:name,
          id:id,
          _openid:openid
        }
      })
      return {
        type:ittype,
        name:name,
        id:id,
        _openid:openid
      }
    }
    

  }
  else if(event.type == 'getitem')
  {
    return db.collection('nftitem')
    .where({
        _openid:openid
      })
    .orderBy('id','asc')
    .get()
  }
  else if(event.type == 'use')
  {
      let item = await db.collection('nftitem')
      .doc(event.id)
      .get()

      let ittype = 'item'
      let name = ''
      let id = ''
      
      switch (item.data.id) {
          case 'fishbox':
            name = '小鱼干x500'
            id = 'fish'
            await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                fish:db.command.inc(500),
              }
            })
            break;
        case 'canbox':
            name = '鱼罐头x500'
            id = 'can'
            await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                can:db.command.inc(500),
              }
            })
            break;
        case 'coinbox':
            name = '猫猫币x500'
            id = 'coin'
            await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                coin:db.command.inc(500),
              }
            })
            break;
        case 'fubenquan':
            name = '暂时无法使用'
            id = 'fubenquan'
            return {
                type:ittype,
                name:name,
                id:id,
              }
            break;
          default:
              break;
      }
      await db.collection('nftitem')
      .doc(event.id)
      .remove()


      return {
        type:ittype,
        name:name,
        id:id,
      }
  }
  else if(event.type == 'sell')
  {
      let item = await db.collection('nftitem')
      .doc(event.id)
      .get()

      let ittype = 'item'
      let name = '猫猫币x1000'
      let id = 'coin'
 
            await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                coin:db.command.inc(1000),
              }
            })
      
      await db.collection('nftitem')
      .doc(event.id)
      .remove()


      return {
        type:ittype,
        name:name,
        id:id,
      }
  }
  else if(event.type == 'zhuangbei')
  {

    if(event.caowei == 'caowei1')
    {
      await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                caowei1:event.id
              }
            })
    }
    else if(event.caowei == 'caowei2')
    {
      await db.collection('cat')
            .where({_openid:openid})
            .update({
              data: {
                caowei2:event.id
              }
            })
    }
    else
    {
      
    }
    return 'ok'
    
  }
  else
  {
    return '参数错误'
  }
  
}
