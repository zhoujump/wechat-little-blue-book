// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  let retres
  switch (event.type) {
    case 'creat':
      await db.collection('posts').add({
        data: {
          title:event.title,
          post:null,
          _openid:openid
        }
      })
      .then(res => {
        retres = res._id
      })
      break;
  
    case 'post':
      await db.collection('posts').doc(event.id).update({
        data: {
          post:event.post
        }
      })
      .then(res => {
        retres = res
      })
      break;

    default:
      break;
  }

  return retres
}
