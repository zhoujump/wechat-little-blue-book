// 云函数入口文件
const cloud = require('wx-server-sdk')
var request = require('request');
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  if(event.type == 'add')
  {

    let user = await db.collection('userinfo')
    .where({_openid:openid})
    .get()

    let seesee = await db.collection('goods')
    .where({_id:event.id})
    .get()


    if(user.data[0].money < seesee.data[0].newprice)
    {
      return '余额不足'
    }

    if(seesee.data[0].num <= 0)
      {
        return '没货了'
      }
    else
      {
        await db.collection('goods')
        .doc(event.id)
        .update({
        data: {
          num:db.command.inc(-1)
        }
      })

        let goods = await db.collection('cart')
        .where({
            _id:event.id,
          })
        .get()

          delete goods.data[0]._id
          goods.data[0].state = "已支付"
          
        await db.collection('orders')
        .add({
            data: {
                ...goods.data[0],
            }
          })
    
        await db.collection('cart')
        .where({
          _id:event.id,
          _useropenid:openid
        })
        .remove()
        
        await db.collection('userinfo')
        .where({
          _openid:openid
        })
        .update({
            data: {
              money:db.command.inc(-seesee.data[0].newprice)
            }
          })

          return 'ok'
      }
  }
  else if(event.type == 'sent')
  {
    await db.collection('orders')
    .where({
      _id:event.id,
      _openid:openid
    })
    .update({
        data: {
          state:'已发货'
        }
      })
      return 'ok'
  }
  else if(event.type == 'comf')
  {
    await db.collection('orders')
    .where({
      _id:event.id,
      _useropenid:openid
    })
    .update({
        data: {
          state:'已收货'
        }
    })
    
    let goods = await db.collection('orders')
    .where({
      _id:event.id,
      _useropenid:openid
    })
    .get()

    let price = goods.data[0].newprice - 0
    let id = goods.data[0]._openid

    await db.collection('userinfo')
    .where({
      _openid:id
    })
    .update({
        data: {
          money:db.command.inc(price)
        }
      })

    return 'ok'
  }
  else if(event.type == 'pay')
  {

    
    let addpay = await db.collection('pay')
        .add({
            data: {
                _openid:openid,
                price:event.price
            }
          })
    addpay._id

    const crypto = require('crypto')

// 商户号和通信密钥
const app_id = '2312243095'
const key = 'c8bed5ff83ea7c876a3d42adea5ec782'

// 排序后转换为字符串
const toQueryString = (obj) => Object.keys(obj)
  .filter(key => key !== 'sign' && obj[key] !== undefined && obj[key] !== '')
  .sort()
  .map(key => {
    if (/^http(s)?:\/\//.test(obj[key])) { return key + '=' + encodeURI(obj[key]) } 
        else { return key + '=' + obj[key] }
  })
  .join('&')

// md5
const md5 = (str, encoding = 'utf8') => crypto.createHash('md5').update(str, encoding).digest('hex')

// 构造请求数据
let params = {
  'amount': parseInt(event.price * 100),
  'out_trade_no': addpay._id,
  'app_id': '2312243095'
}

params = toQueryString(params)
params += '&key=' + key

// 计算出最终签名
const sign = md5(params).toUpperCase()

    return new Promise((resolve, reject) => {
      request({
          url: "https://open.h5zhifu.com/api/miniapp",
          method: "POST",
          json: true,
          body: {
              "app_id":2312243095,
              "out_trade_no":addpay._id,
              "description":"微信支付",
              "pay_type":"wechat",
              "amount":parseInt(event.price * 100),
              "notify_url":"https://h5zhifu.com/",
              "sign":sign
          },
          headers: {
              "content-Type": "application/json",
          },
      }, function (error, response, body) {
          console.log("响应"+body)
          resolve(body)
          if (!error && response.statusCode == 200) {
              try {
  
              } catch (e) {
                  reject()
              }
          }
      })
  })

  
  }
  else if(event.type == 'get')
  {
    let buy = await db.collection('orders')
    .where({
        _useropenid:openid
      })
    .get()

    let sell = await db.collection('orders')
    .where({
        _openid:openid
      })
    .get()

    return {buy:buy,sell:sell}
  }
  else if(event.type == 'charge')
  {
    await db.collection('userinfo')
    .where({
      _openid:openid
    })
    .update({
        data: {
          money:db.command.inc(event.price)
        }
      })

    return 'ok'
  }
  else if(event.type == 'kejin')
  {
    let user = await db.collection('userinfo')
    .where({_openid:openid})
    .get()

    if(user.data[0].money < (event.price -0))
    {
      return '余额不足'
    }

    await db.collection('userinfo')
        .where({
          _openid:openid
        })
        .update({
            data: {
              money:db.command.inc(-(event.price - 0))
            }
          })
    await db.collection('cat')
    .where({
      _openid:openid
    })
    .update({
        data: {
          coin:db.command.inc((event.price - 0)*100)
        }
      })

      return {
        type:'item',
        name:'猫猫币x'+event.price*100,
        id:'coin',
      }
        
  }
  else
  {
  
  }
  
}