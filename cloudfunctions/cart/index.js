// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  if(event.type == 'add')
  {
    let goods = await db.collection('goods')
    .where({
        _id:event.id
      })
    .get()

      goods.data[0]._useropenid = openid
      goods.data[0].num = 0
      goods.data[0].check = false

    await db.collection('cart')
    .add({
        data: {
            ...goods.data[0],
        }
      })
      return 'ok'
  }
  else if(event.type == 'get')
  {
    return await db.collection('cart')
    .where({_useropenid:openid})
    .get()
  }
  else if(event.type == 'del')
  {
    
  }
  else if(event.type == 'update')
  {
  
  }
  else if(event.type == 'add')
  {
 
  }
  else
  {
  
  }
  
}