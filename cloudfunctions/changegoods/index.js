// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  if(event.type == 'creat')
  {
    return db.collection('goods')
    .add({
        data: {
          _openid:openid,
          price:event.price,
          newprice:event.newprice,
          shoptext:event.shoptext,
          shoptitle:event.shoptitle,
          num:0
        }
      })
  }
  else if(event.type == 'get')
  {
    return db.collection('goods')
    .where({
        _openid:event.openid
      })
    .skip((event.page - 1) * 10)
    .limit(10)
    .get()
  }
  else if(event.type == 'del')
  {
    return await db.collection('goods')
    .doc(event.id)
    .remove()
  }
  else if(event.type == 'update')
  {
    return await db.collection('goods')
    .doc(event.id)
    .update({
        data: {
          _openid:openid,
          price:event.price,
          newprice:event.newprice,
          shoptext:event.shoptext,
          shoptitle:event.shoptitle,
          num:0
        }
      })
  }
  else if(event.type == 'add')
  {
    return await db.collection('goods')
    .doc(event.id)
    .update({
        data: {
          num:db.command.inc(event.num - 0)
        }
      })
  }
  else
  {
    return db.collection('goods')
    .where({
        _id:event.id
      })
    .get()
  }
  
}